#include <iostream>

// У нас имеется интерфейс для глаза
struct Eye
{
    // [1] Просто переопределяем виртуальный метод
    virtual void open() = 0;
    virtual void look() = 0;
    /* [3] В данном случае у нас есть невиртуальный метод,
     * который внутри себя вызывает виртуальный метод.
     * Для корректной работы, мы сделаем метод закрытым и чисто виртуальным,
     * что-бы класс наследник его переопределил, но его интерфейс был
     * доступен через базовый класс
    */
    void close() { return eyeClose(); } // [3]
protected:
    // Метод будет реализован в классе наследнике и будет доступен
    // через другой метод в базовом классе.
    virtual void eyeClose() = 0; // [3.1]
};

// У нас имеется интерфейс для рта
struct Mouth
{
    // [2] Так как у нас будет конфликт интерфейсов с классом Eye, нам необходимо
    // Изменить сигнатуру метода.
    virtual void open(int) = 0; // [2]
    virtual void eat() = 0;
    virtual void say() = 0;
    void close() { return mouthClose(); } // [3]
private:
    virtual void mouthClose() = 0; // [3.1]
};

/* Делаем циклопа который умеет видеть и кушать
 * Проблемой является сходесть интерфейсов глаза и рта
 * Мы их можем открывать и закрывать.
 * Стоит отметить что класс циклопа только переопределяет методы базовых классов и
 * не имеет собственного интерфейся для общения с пользователем. Попытка их
 * вызвать из экземпляра класса, без указания базового, вызовет ошибку компиляции
*/
class Cyclop : public Eye, public Mouth
{
    // Eye
    void open() override { std::cout << "eye open" << std::endl; } // [1]
    void look() override { std::cout << "eye look" << std::endl; }
    void eyeClose() override { std::cout << "eye close" << std::endl; }

    // Mounth
    void open(int) override { std::cout << "mounth open" << std::endl; }
    void eat() override { std::cout << "mounth eat" << std::endl; }
    void say() override { std::cout << "mounth say" << std::endl; }
    void mouthClose() override { std::cout << "mounth close" << std::endl; }
};

/* Стоит обратить внимание, что в предыдущем примере мы
 * смогли реализовать единый интерфейс.
 * Для обращения к глазам и рта циклопа.
 * Сейчас же, программа работает как мы хотели.
*/
void lookForBanana(Eye *eye)
{
    eye->open(); // [1]
    eye->look();
    eye->close(); // [3]
}

void eatBanana(Mouth *mouth)
{
    mouth->open(0); // [2]
    mouth->eat();
    mouth->say();
    mouth->close();
}

int main()
{
    Cyclop *cyclop = new Cyclop;

    lookForBanana(cyclop);
    eatBanana(cyclop);

    delete cyclop;

    return 0;
}

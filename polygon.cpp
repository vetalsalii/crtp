#include <cmath>
#include <iostream>

class RegularPolygon {
    const int n;
public:
    RegularPolygon(int n_) : n(n_) {}
    virtual ~RegularPolygon() {}

    virtual int area(int a) = 0;

    int perimeter(int a) {
        return n * a;
    }
};

class Square : public RegularPolygon
{
public:
    Square() : RegularPolygon(4) {}

    int area(int a) override {
        return a * a;
    }
};
class Triangle : public RegularPolygon
{
public:
    Triangle() : RegularPolygon(3) {}

    int area(int a) override {
        return a * a * std::sqrt(3) / 4;
    }
};

int main(int argc, char*[])
{
    RegularPolygon *s = nullptr;

    if (argc % 2)
        s = new Square;
    else
        s = new Triangle;

    s->area(10);
    int r = s->perimeter(10);

    delete s;

    return r;
}

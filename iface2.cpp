#include <iostream>

// У нас имеется интерфейс для глаза
struct Eye
{
    virtual void open() = 0;
    virtual void look() = 0;
    virtual void close() = 0;
};

// У нас имеется интерфейс для рта
struct Mouth
{
    virtual void open() = 0;
    virtual void eat() = 0;
    virtual void say() = 0;
    virtual void close() = 0;

};

/* Делаем циклопа который умеет видеть и кушать
 * Проблемой является сходесть интерфейсов глаза и рта
 * Мы их можем открывать и закрывать. Мы не можем их оба
 * переопределить в классе циклопа так как нарушим правило ODR
 * Решение проблемы в файле iface3.cpp
*/
class Cyclop : public Eye, public Mouth
{
public:
    // Eye
    void open() override { std::cout << "eye open" << std::endl; };
    void look() override { std::cout << "eye look" << std::endl; }
    void close() override { std::cout << "eye close" << std::endl; }

    // Mounth
    // void open() override { std::cout << "mounth open" << std::endl; };
    void eat() override { std::cout << "mounth eat" << std::endl; }
    void say() override { std::cout << "mounth say" << std::endl; }
    // void close() override { std::cout << "mounth close" << std::endl; }
};

void lookForBanana(Eye *eye)
{
    eye->open();
    eye->look();
    eye->close();
}

void eatBanana(Mouth *mouth)
{
    mouth->open();
    mouth->eat();
    mouth->say();
    mouth->close();
}

int main()
{
    Cyclop *cyclop = new Cyclop;

    lookForBanana(cyclop);
    eatBanana(cyclop);

    delete cyclop;

    return 0;
}

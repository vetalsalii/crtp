#include <iostream>
#include <string>

/* У нас имеется 2 одинаковых метода в различных классах
 * и хотелось бы как-то вынести их в один общий класс.
 * Мы можем создать класс A, в нем создать виртуальный метод
 * и каждый класс который унаследуется от A, переопределит виртуальный метод.
 * В классе появится указатель на виртуальную таблицу,
 * в которой будет содержаться указатель на функцию.
 * Это приведет к тому что программа не будет оптимизирована компилятором,
 * т.к. создание класса и таблицы происходит на run time.
*/
struct Author
{
    Author(const std::string &r, const std::string &e)
        : russian(r)
        , english(e)
    {}
    std::string russian;
    std::string english;

    std::string formatted() {
        return russian + " (" + english + ")";
    }
};

struct Movie
{
    Movie(const std::string &r, const std::string &e)
        : russian(r)
        , english(e)
    {}
    std::string russian;
    std::string english;

    std::string formatted() {
        return russian + " (" + english + ")";
    }
};

int main()
{
    Author author("Иван", "Ivan");
    Movie movie("Титаник", "Titanic");

    std::cout << author.formatted() << std::endl;
    std::cout << movie.formatted() << std::endl;

    return 0;
}

#include <iostream>

// Dual interface pattern

template <typename T>
struct Eye
{
    void open() { return static_cast<T*>(this)->eye_open(); }
    void look() { return static_cast<T*>(this)->eye_lock(); }
    void close() { return static_cast<T*>(this)->eye_close(); }
};

template <typename T>
struct Mouth
{
    void open() { return static_cast<T*>(this)->mouth_open(); }
    void eat() { return static_cast<T*>(this)->mouth_eat(); }
    void say() { return static_cast<T*>(this)->mouth_say(); }
    void close() { return static_cast<T*>(this)->mouth_close(); }
};

struct Cyclop : public Eye<Cyclop>, public Mouth<Cyclop>
{
    // Eye
    void eye_open() { std::cout << "eye open" << std::endl; }
    void eye_lock() { std::cout << "eye look" << std::endl; }
    void eye_close() { std::cout << "eye close" << std::endl; }

    // Mounth
    void mouth_open() { std::cout << "mounth open" << std::endl; }
    void mouth_eat() { std::cout << "mounth eat" << std::endl; }
    void mouth_say() { std::cout << "mounth say" << std::endl; }
    void mouth_close() { std::cout << "mounth close" << std::endl; }
};

template <typename T>
void lookForBanana(Eye<T> *eye)
{
    eye->open();
    eye->look();
    eye->close();
}

template <typename T>
void eatBanana(Mouth<T> *mouth)
{
    mouth->open();
    mouth->eat();
    mouth->say();
    mouth->close();
}

int main()
{
    Cyclop *cyclop = new Cyclop;

    lookForBanana(cyclop);
    eatBanana(cyclop);

    delete cyclop;

    return 0;
}

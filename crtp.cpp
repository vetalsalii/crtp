#include <iostream>

// Dual interface pattern

struct Cyclop;

struct Eye
{
    void open() { /* return static_cast<Cyclop*>(this)->eyeOpen(); */ }
};

struct Cyclop : public Eye
{
    void eyeOpen() { std::cout << "eye open" << std::endl; }
};

void lookForBanana(Eye *eye)
{
    eye->open();
}

int main()
{
    Cyclop *cyclop = new Cyclop;

    lookForBanana(cyclop);


    delete cyclop;

    return 0;
}

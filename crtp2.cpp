#include <iostream>

template <typename T>
struct Eye
{
    void open() { return static_cast<T*>(this)->eyeOpen(); }
};

struct Cyclop : public Eye<Cyclop>
{
    void eyeOpen() { std::cout << "eye open" << std::endl; }
};

template <typename T>
void lookForBanana(Eye<T> *eye)
{
    eye->open();
}

int main()
{
    Cyclop *cyclop = new Cyclop;

    lookForBanana(cyclop);

    delete cyclop;

    return 0;
}

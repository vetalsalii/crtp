#include <iostream>

// У нас имеется интерфейс для глаза
struct Eye
{
    virtual void open() = 0;
    virtual void look() = 0;
    virtual void close() = 0;
};

// Циклоп реализует интерфейс своего зрения, мы можем здесь реализовать другой класс
// например видеокамеры, она включается, смотрит/записывает и выключается
class Cyclop : public Eye
{
public:
    void open() override { std::cout << "eye open" << std::endl; };
    void look() override { std::cout << "eye look" << std::endl; }
    void close() override { std::cout << "eye close" << std::endl; }
};

// Функция в которой мы используем наши переопределённые методы
void lookForBanana(Eye *eye)
{
    eye->open();
    eye->look();
    eye->close();
}

int main()
{
    Cyclop *cyclop = new Cyclop;

    lookForBanana(cyclop);

    delete cyclop;

    return 0;
}
